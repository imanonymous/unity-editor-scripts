﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;

[Serializable]
public class SphericalObject
{
    public string prefab;
    public Vector3 position;
    public Vector3 scaling;
    public Quaternion rotation;
    public List<SphericalObject> gameobjects;

    public void fromTransform(Transform t)
    {
        Vector3 pos = t.position;
        position = new Vector3(pos.z, pos.y, pos.x);
        Vector3 scl = t.lossyScale;
        scaling = new Vector3(scl.z, scl.y, scl.x);
        Quaternion rot = t.rotation;
        rotation = new Quaternion(rot.z, rot.y, rot.x, rot.w);
    }
}

public class SphericalUnityScripts : EditorWindow
{
    [MenuItem("Assets/Spherical Age/Export Object to JSON")]
    static void exportPrefabs()
    {
        if (!Selection.activeGameObject)
        {
            EditorUtility.DisplayDialog("No object selected", "Please select an object.", "Cancel");
            return;
        }

        GameObject gameobject = Selection.activeGameObject;
        SphericalObject sphericalObject = traverse(gameobject);
        string json = JsonUtility.ToJson(sphericalObject);

        json = json.Replace("\"x\"", "\"0\"");
        json = json.Replace("\"y\"", "\"1\"");
        json = json.Replace("\"z\"", "\"2\"");
        json = json.Replace("\"w\"", "\"3\"");
        json = json.Replace(" (UnityEngine.GameObject)", "");

        String path = EditorUtility.SaveFilePanel("Select file", "", "data.json", "json");

        if (path.Length != 0)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(json);
                }
            }
        }
    }

    static SphericalObject traverse(GameObject gameobject)
    {
        SphericalObject sphericalObject = new SphericalObject();
        sphericalObject.fromTransform(gameobject.transform);
        sphericalObject.gameobjects = new List<SphericalObject>();

        foreach(Transform child in gameobject.transform)
        {
            UnityEngine.Object prefab = PrefabUtility.GetPrefabParent(child.gameObject);

            if (prefab != null)
            {
                SphericalObject childObject = new SphericalObject();
                childObject.prefab = prefab.ToString();
                childObject.fromTransform(child);

                sphericalObject.gameobjects.Add(childObject);
            }
        }

        Debug.Log(sphericalObject.gameobjects.Count);

        return sphericalObject;
    }

    [Serializable]
    public class SphericalTerrain
    {
        public int width;
        public int height;
        public List<float> heightData;
    }

	[MenuItem("Assets/Spherical Age/Export Terrain")]
    static void exportTerrain()
    {
        TerrainData terraindata = null;
        Terrain terrain = null;

        if (Selection.activeGameObject)
            terrain = Selection.activeGameObject.GetComponent<Terrain>();

        if (!terrain)
        {
            terrain = Terrain.activeTerrain;
        }
        if (terrain)
        {
            terraindata = terrain.terrainData;
        }
        if (terraindata == null)
        {
            EditorUtility.DisplayDialog("No terrain selected", "Please select a terrain.", "Cancel");
            return;
        }

        float[,] rawHeights = terraindata.GetHeights(0, 0, terraindata.heightmapWidth, terraindata.heightmapHeight);

        SphericalTerrain sphericalTerrain = new SphericalTerrain();
        sphericalTerrain.width = terraindata.heightmapWidth;
        sphericalTerrain.height = terraindata.heightmapHeight;

        float[] tmp = new float[rawHeights.GetLength(0) * rawHeights.GetLength(1)];
        for (int x = 0; x < rawHeights.GetLength(0); x++)
        {
            for (int y = 0; y < rawHeights.GetLength(1); y++)
            {
                tmp[terraindata.heightmapWidth * y + x] = rawHeights[x, y] * terraindata.heightmapScale.y;
            }
        }
        sphericalTerrain.heightData = new List<float>(tmp);

        float[,,] alphadata = terraindata.GetAlphamaps(0, 0, terraindata.alphamapWidth, terraindata.alphamapHeight);
        Texture2D alphamap = new Texture2D(terraindata.alphamapWidth, terraindata.alphamapHeight, TextureFormat.ARGB32, false);
        for (int y = 0; y < terraindata.alphamapHeight; y++)
        {
            for (int x = 0; x < terraindata.alphamapWidth; x++)
            {
                float r = terraindata.alphamapLayers >= 1 ? alphadata[x, y, 0] : 0.0f;
                float g = terraindata.alphamapLayers >= 2 ? alphadata[x, y, 1] : 0.0f;
                float b = terraindata.alphamapLayers >= 3 ? alphadata[x, y, 2] : 0.0f;
                float a = terraindata.alphamapLayers >= 4 ? alphadata[x, y, 3] : 0.0f;

                Color c = new Color(r, g, b, a);
                alphamap.SetPixel(x, terraindata.alphamapHeight - 1 - y, c);
            }
        }
        alphamap.Apply();

        String path = EditorUtility.SaveFilePanel("Select file", "", "data.json", "json");

        if (path.Length != 0)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(JsonUtility.ToJson(sphericalTerrain));
                }
            }

            String splatPath = Path.GetDirectoryName(path) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(path) + "_splatmap.png";
            File.WriteAllBytes(splatPath, alphamap.EncodeToPNG());

            EditorUtility.DisplayDialog("Done", "Export is done!", "OK");
        }
    }

    [MenuItem("Assets/Spherical Age/Rotate Terrain")]
    static void rotateTerrain()
    {
        TerrainData terraindata = null;
        Terrain terrain = null;

        if (Selection.activeGameObject)
            terrain = Selection.activeGameObject.GetComponent<Terrain>();

        if (!terrain)
        {
            terrain = Terrain.activeTerrain;
        }
        if (terrain)
        {
            terraindata = terrain.terrainData;
        }
        if (terraindata == null)
        {
            EditorUtility.DisplayDialog("No terrain selected", "Please select a terrain.", "Cancel");
            return;
        }

        float[,] originalheights = terraindata.GetHeights(0, 0, terraindata.heightmapWidth, terraindata.heightmapHeight);
        float[,] rawheights = new float[originalheights.GetLength(1), originalheights.GetLength(0)];

        for (int y = 0; y < rawheights.GetLength(1); y++)
        {
            for (int x = 0; x < rawheights.GetLength(0); x++)
            {
                rawheights[y, rawheights.GetLength(0) - 1 - x] = originalheights[x, y];
            }

        }

        terraindata.SetHeights(0, 0, rawheights);

    }

    [MenuItem("Assets/Spherical Age/Rotate Texture")]
    static void rotateTexture()
    {
        Texture2D texture = Selection.activeObject as Texture2D;
        if (texture == null)
        {
            EditorUtility.DisplayDialog("Select Texture", "You Must Select a Texture first!", "Ok");
            return;
        }

        Color32[] pixels = texture.GetPixels32();
        pixels = RotateMatrix(pixels, texture.width);
        texture.SetPixels32(pixels);

        texture.Apply();
    }

    static Color32[] RotateMatrix(Color32[] matrix, int n)
    {
        Color32[] ret = new Color32[n * n];

        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                ret[i * n + j] = matrix[(n - j - 1) * n + i];
            }
        }

        return ret;
    }

}